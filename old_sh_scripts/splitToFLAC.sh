#!/bin/sh
#
# Split single file album into multiple FLAC files, using CUE file.
#
# Arguments:
# 1) root directory
#
# This directory must contain a .cue file.
#
# Ex.: split2flac album_x_single_file.wv -of '@track @title.@ext' -nask -c cover.jpg -cue album_x_single_file.wv.cue
#
# Alain Poirier, 2015
#

function printConsoleAndLog()
{
  stp="$1"
  echo -e "${stp}"
  echo -e "${stp}" >> $logFile
}

function printConsoleAndLogBold()
{
  stp="$1"
  echo -e "\e[1m${stp}\e[0m"
  echo -e "${stp}" >> $logFile
}

#
# arg: .cue file
# return audio file referred to by the given .cue file
function findSourceFile()
{
  cueFile="$1"
  printConsoleAndLog "\n.cue file = ${cueFile}"

  # Find audio file related to the .cue file
  sourceFile=`echo ${cueFile} | sed 's/.cue//g'`
  
  if [ -e $sourceFile ];
  then
    printConsoleAndLog "source file = ${sourceFile}";
  else
#     sourceFile=`find . -name '"${sourceFile}"*' | grep -v '.cue'`
    sourceFile=`ls "${sourceFile}"* | grep -v '.cue'`;
    printConsoleAndLog "source file = ${sourceFile}";
  fi
}


#  --  Main  --

# change file name separator
oldIFS=$IFS
IFS=$'\n'

root=`readlink -f "$1"`   # readlink -f so a relative path will be converted to a full path
cd "$root"
start=`date +%F'T'%T`
logFile="${root}/splitToFLAC_"`date +%F'_'%H%m%S`".log"
printConsoleAndLog `date +%F'_'%H%m%S`
printConsoleAndLog "\nlog file = ${logFile}"
printConsoleAndLog "\nroot directory = $root"
printConsoleAndLog "\nSize of subdirectories:\n"
printConsoleAndLog "`du -hc *`"

cueFile=`ls *cue`
findSourceFile $cueFile
# ${sourceFile} declared in function findSourceFile, and shared here! By Design. 
printConsoleAndLog "source file = ${sourceFile}";

if [ -e cover.jpg ];
then
  printConsoleAndLog "Found cover.jpg";
else
  printConsoleAndLog "File cover.jpg NOT FOUND"; exit 1;
fi
coverFile=`ls cover.jpg`

printConsoleAndLog "\nNow call split2flac ...\n\n"

# Ex.: split2flac album_x_single_file.wv -of '@track @title.@ext' -nask -c cover.jpg -cs 700x700 -cue album_x_single_file.wv.cue
split2flac "${sourceFile}" -of '@track @title.@ext' -nask -c cover.jpg -cs 700x700 -cue "${cueFile}"

end=`date +%F'T'%T`
printConsoleAndLog "\n\n"`date +%F'_'%H%m%S`
# calculate and print complete batch execution time
c=`date -u -d $start +%s`
d=`date -u -d $end +%s`
e=$(( $d - $c ))
x=$(( $e / 60 ))
printConsoleAndLog "Total batch took $e seconds (or $x minutes)"

IFS=$oldIFS