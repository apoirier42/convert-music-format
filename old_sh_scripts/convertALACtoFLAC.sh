#!/bin/sh
#
# Convert audio files from ALAC to FLAC format, recursively from a root directory.
#
# Arguments:
# 1) root directory
#
# Alain Poirier, 2014
#

# extension of source files
export sourceFormat="m4a"
# extension of destination files
export destinationFormat='flac'
# temporary work directory where new files are created
export workDir=./out_conversion

function printConsoleAndLog()
{
  stp="$1"
  echo -e "${stp}"
  echo -e "${stp}" >> $logFile
}

function printConsoleAndLogBold()
{
  stp="$1"
  echo -e "\e[1m${stp}\e[0m"
  echo -e "${stp}" >> $logFile
}

# return true (1) if the actual directory contains files ending with #sourceFormat, else 0.
function containsSourceMusic()
{
  if ls *"$sourceFormat" &> /dev/null; then
    #echo "yes...........";
    return 1
  else
    #echo "no music!!!";
    return 0
  fi
}

# convert audio files in the given directory
function convertMusic()
{
  ddir="$1"
  echo -e "\nWill convert music in $ddir"

  # remove useless files that exist sometimes
  if ls .*DS_Store &> /dev/null; then rm .*DS_Store; fi;
  if ls .fuse* &> /dev/null; then rm .fuse*; fi;

  # clear work directory if it exists, and create a new one
  if [[ -d $workDir ]]; then rm -r $workDir; fi; mkdir $workDir
  echo "workDir = $workDir"

  ##   <<< <<< <<< <<< <<< <<< <<< <<< <<< <<< 
  ## Convert audio files using ffmpeg command.
  ## If using GNU parallel command, option -j defines the number
  ## of threads allowed.
  ##   <<< <<< <<< <<< <<< <<< <<< <<< <<< <<< 
   # time parallel -j 6 'f={}; ffmpeg -i "$f" -loglevel debug -sample_fmt s16 -ar 44100 "${workDir}"/"${f%.$sourceFormat}.$destinationFormat"' ::: *.$sourceFormat
  time parallel -j 4 'f={}; ffmpeg -i "$f" -sample_fmt s16 -ar 44100 "${workDir}"/"${f%.$sourceFormat}.$destinationFormat" &> /dev/null' ::: *.$sourceFormat

  
  # rename FLAC files
  EXTENSION=flac

  for f in *.$EXTENSION
  do
    echo -n "$f  ->  "
    name=`echo -e $f | sed 's#\([0-9]\{1,2\}\)\([-._ ]*\)\(.*\)#\1 \3#g'`
    echo -e "$name"
    mv "$f" "$name"
  done  
  # END of rename FLAC files
  
  
  # remove original files.   ## FIXME verify if process completed successfuly, i.e. destination files exist and are not empty!!
  # rm *.$sourceFormat
  # move 'destination' files to the orinal album directory
  mv $workDir/* $ddir
}


# Print statistics on audio files (given extension) in the actual directory
function printFileStats()
{
  extension="$1"
  printConsoleAndLog "`du -hc *.${extension}`"
  printConsoleAndLog "`ls *.${extension} | wc -l` file(s)"
}

# If the given directory contains files of interest (#sourceFormat), 
# execute the conversion and move/delete files, etc.
function processDir()
{
  ddir="$1"
  cd "$ddir"
#   echo -e "\n\nDEBUG process $ddir\n"

  containsSourceMusic "$ddir"
  contains=$?

  if [[ $contains -eq 1 ]]; then
    printConsoleAndLog "\n"`date +%F'_'%H%m%S`
    printConsoleAndLogBold "\nINFO process $ddir\n"
    printConsoleAndLog "Original files:"
    printFileStats "${sourceFormat}"

    convertMusic "$ddir"

    printConsoleAndLog "\nNew files:"
    printFileStats "${destinationFormat}"    
    # remove the temporary work directory
    rm -r $workDir
  fi
}


#  --  Main  --

root=`readlink -f "$1"`   # readlink -f so a relative path will be converted to a full path
cd "$root"
start=`date +%F'T'%T`
logFile="${root}/conversionALACtoFLAC_"`date +%F'_'%H%m%S`".log"
printConsoleAndLog `date +%F'_'%H%m%S`
printConsoleAndLog "\nlog file = ${logFile}"
printConsoleAndLog "\nroot directory = $root"
printConsoleAndLog "\nSize of subdirectories:\n"
printConsoleAndLog "`du -hc *`"

oldIFS=$IFS
IFS=$'\n'
# loop in all subdirectories searching for audio files!
for d in `find "$root" -type d | sort`; do
  #echo "$d";
  processDir $d
done
IFS=$oldIFS

echo -e "\n\n------  Conversion completed  ------\n"
cd "$root"
printConsoleAndLog "\nSize of subdirectories:\n"
printConsoleAndLog "`du -hc *`"

end=`date +%F'T'%T`
printConsoleAndLog "\n\n"`date +%F'_'%H%m%S`
# calculate and print complete batch execution time
c=`date -u -d $start +%s`
d=`date -u -d $end +%s`
e=$(( $d - $c ))
x=$(( $e / 60 ))
printConsoleAndLog "Total batch took $e seconds (or $x minutes)"
