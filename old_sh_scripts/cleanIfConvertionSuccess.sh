#!/bin/bash
#
# Remove source file if the destination file's size is greater than zero, 
# recursively from a root directory.
#
# Arguments:
# 1) root directory
#
# Alain Poirier, 2015
#

# extension of source files
export sourceFormat="flac"
# extension of destination files
export destinationFormat='m4a'

function printConsoleAndLog()
{
  stp="$1"
  echo -e "${stp}"
  echo -e "${stp}" >> $logFile
}

function printConsoleAndLogBold()
{
  stp="$1"
  echo -e "\e[1m${stp}\e[0m"
  echo -e "${stp}" >> $logFile
}

# return true (1) if the actual directory contains files ending with #sourceFormat, else 0.
function containsSourceMusic()
{
  if ls *"$sourceFormat" &> /dev/null; then
    #echo "yes...........";
    return 1
  else
    #echo "no music!!!";
    return 0
  fi
}


# Print statistics on audio files (given extension) in the actual directory
function printFileStats()
{
  extension="$1"
  printConsoleAndLog "`du -hc *.${extension}`"
  printConsoleAndLog "`ls *.${extension} | wc -l` file(s)"
}

# If the given directory contains files of interest (#sourceFormat), 
# execute the conversion and move/delete files, etc.
function processDir()
{
  ddir="$1"
  cd "$ddir"
#   echo -e "\n\nDEBUG process $ddir\n"

  echo "sourceFormat = " $sourceFormat

  for f in `ls *"${sourceFormat}"`; do
#   for f in `find *"${sourceFormat}"`; do
#     echo "f = " $f
    name=`echo $f | sed "s/[.]${sourceFormat}//g"` # must use double quotes to preserve whitespaces in sed pattern
    echo "  name = " $name
    
    out=$name"."$destinationFormat
#     echo "  out  = " $out
    
    if [[ `ls -s $out | grep -o '^[0-9]'` == "0" ]]; then
      echo "  size is zero";
    else
      echo "  size is positive";
      rm $f;
    fi
    
  done
}


#  --  Main  --

root=`readlink -f "$1"`   # readlink -f so a relative path will be converted to a full path
cd "$root"
start=`date +%F'T'%T`

oldIFS=$IFS
IFS=$'\n'
# loop in all subdirectories searching for audio files!
for d in `find "$root" -type d | sort`; do
  #echo "$d";
  processDir $d
done
IFS=$oldIFS

