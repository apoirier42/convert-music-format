#!/bin/bash
#
# Updates music collection from PC hard drive to USB drive.
#
# arguments:
# - destination root music directory, containing "classical", "jazz", etc.
#
# Alain Poirier, 2016-03-28

DIR_SOURCE=/mnt/dat8t/music/

destinationDir=$1

echo -e "\n"`date +%F' '%H:%M:%S`
echo -e "\nWill synchonize music collection between the two following root directories:\n"
echo -e "Source directory      = $DIR_SOURCE"
echo -e "Destination directory = $destinationDir\n"

function printTotalSize()
{
  echo "Total size of $1 = "`du -hc $1 | tail -n1 | sed 's/total//g'`
}

# loop on music type directories, e.g., classical, rock, etc. By convention directories z* contain scripts or TODO data
for sourceDir in $DIR_SOURCE/[a-y]*
do
  echo -e "\n"`date +%F' '%H:%M:%S`
  printTotalSize $sourceDir
  echo "Synchonize ..."
  
   rsync --update --delete --recursive --archive --size-only $sourceDir $destinationDir
#  rsync --update --delete --recursive --archive $sourceDir $destinationDir
  
done

echo -e "\n"`date +%F' '%H:%M:%S`
