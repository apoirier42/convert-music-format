EXTENSION=flac

d=`readlink -f $1`
echo -e "directory = $d\n"
cd "$d"

for f in *.$EXTENSION
do
  echo -n "$f  ->  "
  name=`echo -e $f | sed 's#\([0-9]\{1,2\}\)\([-._ ]*\)\(.*\)#\1 \3#g'`
  echo -e "$name"
  mv "$f" "$name"
done  
