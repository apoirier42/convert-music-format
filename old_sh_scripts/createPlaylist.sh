#!/bin/bash
#
# Alain Poirier, 2012-11-16
#

usage='
Create a playlist (m3u) for the NAS on the Netgear router (\\readyshare\USB_Storage\music),
  based on the fact that it contains an exact copy of /mnt/datab/musique.
Process: list all music files (mp3|ogg|wma|flac) in a given directory and all its sub directories
  and transform the path from /mnt/datab/musique/XYZ/abc.mp3 to \\readyshare\USB_Storage\music\XYZ\abc.mp3 
  for example.

usage: ./createPlaylist.sh <playlist> <directory>+
e.g.:  ./createPlaylist.sh rock.m3u rock
e.g.:  ./createPlaylist.sh jazz.playlist.1.m3u MilesDavis/KindOfBlue BillEvans/YouMustBelieveInSpring

Arguments:
   1 : directory containing music
   2 : name of the output file (the playlist to create)
'

###################################################

ROOT_NAS_DIR="\\\\readyshare\USB_Storage\music"

###################################################

print_usage_exit()
{
  echo "$usage"
  exit 1
}

if [ "$1" == "--help" ]; then
  print_usage_exit
fi

if [ ! $# -ge 2 ]; then
  echo "ERROR: at least 2 arguments needed"
  print_usage_exit
fi

output=$1
shift   # shift args, i.e., the 1st arg is removed from $@

echo "Output file:    $output"

# root_dir=`readlink -f $1`
# root_dir=$1

echo "\$@: $@"
echo "ROOT_NAS_DIR: $ROOT_NAS_DIR"


# Loop through given diretories

ORIG_IFS="$IFS"; ## http://unix.stackexchange.com/questions/9496/looping-through-files-with-spaces-in-the-names
IFS=$'\n'        ## The shell reads the IFS variable, which is set to <space><tab><newline> by default.

echo "#EXTM3U" > $output

for d in $@; do
  echo "exploring directory: $d"
  if [ ! -d $d ]; then
    echo "ERROR: directory $d not found"
    exit 2
  fi

  for musicFile in `find $d -regex ".*\(mp3\|ogg\|wma\|flac\)"`; do
    tmpfn=`echo $musicFile | sed 's/[/]/\\\/g'`
    newname="$ROOT_NAS_DIR\\$tmpfn"

    echo "#EXTINF:0,$musicFile"
    
    echo "#EXTINF:0,"`echo "$musicFile" | grep -o -E "[a-zA-Z0-9 -?_@#&'\.\$\^\*\(\)]+[.](mp3|ogg|flac|wma)"` >> $output;
    echo $newname >> $output
  done
done

IFS="$ORIG_IFS"

