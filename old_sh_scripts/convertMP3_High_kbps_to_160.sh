#!/bin/sh
#
# Convert audio files from MP3 at 200 kbps or more to 160 kbps format, recursively from a root directory.
#
# Arguments:
# 1) root directory
#
# Alain Poirier, 2015
# 


#  --  Main  --

root=`readlink -f "$1"`   # readlink -f so a relative path will be converted to a full path
cd "$root"
echo -e "$root"

# the pattern ** used in a pathname expansion context will match all files and zero or more directories and subdirectories.
shopt -s globstar

parallel 'ffprobe -show_streams -select_streams a:0 {} -v quiet | egrep "bit_rate=[2-9][0-9]{5}" &&
ffmpeg -y -i {} -c:a libmp3lame -b:a 160k {.}.temp.mp3 && mv {.}.temp.mp3 {}' ::: **/*.mp3
