## 1.0.0 (2019-11-09)

- Convert all subdirectories of a given root directory
- Convert directories in parallel
- All those conversions are available:

  - flac to flac
  - flac to m4a
  - flac to mp3
  -  m4a to flac
  -  m4a to mp3
  -  wav to flac
  -  wav to mp3
