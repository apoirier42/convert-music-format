"""
Tests on convert_music.py.
"""

import unittest
from unittest.mock import patch
import re
import os
import sys
import glob
import shutil
import tempfile
from convert_music import build_ffmpeg_command, check_conversion_available, \
                          check_ffmpeg_available, create_work_directory, \
                          delete_original_files, get_all_directories, get_conversions, \
                          main, parse_command_line, process_directory, \
                          run_os_command, \
                          DEFAULT_BIT_RATE, DEFAULT_INPUT, DEFAULT_OUTPUT, WORK_DIRECTORY
import convert_music


DIR_NO_AUDIO_FILE = 'test/test_get_all_directories/sub2'


class TestBuildFFMPEGCommand(unittest.TestCase):
    """
    Test `build_ffmpeg_command`
    """

    def test_unsupported_conversion(self):
        """
        An unsupported conversion causes a SystemExit.
        """
        with self.assertRaises(SystemExit):
            build_ffmpeg_command('/tmp', '/tmp',
                                 input_extension='foo', output_extension='bar', bit_rate=222)

    def test_supported_conversion(self):
        """
        Validate that those conversions are supported, so an ffmpeg command is returned,
        and seems appropriate (check via regex).

        Example of ffmpeg command:
          cd /tmp; for f in *.flac;
          do
            ffmpeg -i "$f" -loglevel quiet  -acodec alac  "/tmp"/"${f%.flac}.m4a";
          done
        """
        for scenario in get_conversions():
            with self.subTest(msg=str(scenario)):
                in_ext = scenario[0]
                out_ext = scenario[1]

                res = build_ffmpeg_command('/tmp', '/tmp',
                                           input_extension=in_ext,
                                           output_extension=out_ext,
                                           bit_rate=222)

                pattern = 'cd.*{}.*ffmpeg.*{}'.format(in_ext, out_ext)
                pat = re.compile(pattern)
                self.assertIsNotNone(pat.match(res))

        # FLAC to MP3 with bit rate
        in_ext = 'flac'
        out_ext = 'mp3'
        bit_rate = 256

        res = build_ffmpeg_command('/tmp', '/tmp',
                                   input_extension=in_ext,
                                   output_extension=out_ext,
                                   bit_rate=bit_rate)

        print(res)

        pattern = 'cd.*{}.*ffmpeg.*[ ]{}k[ ].*{}'.format(in_ext, bit_rate, out_ext)
        pat = re.compile(pattern)
        self.assertIsNotNone(pat.match(res))


class TestDeleteOriginalFiles(unittest.TestCase):
    """
    Test `delete_original_files`.
    """

    ROOT_DIR = os.path.join('.', 'test_delete')

    def setUp(self):
        """
        Create an empty root directory.
        """
        self.tearDown()
        os.mkdir(self.ROOT_DIR, mode=0o755)

    def tearDown(self):
        """
        Delete the root directory.
        """
        if os.path.exists(self.ROOT_DIR):
            shutil.rmtree(self.ROOT_DIR)

    def _create_files(self, in_ext, out_ext, work_directory):
        """
        Create files and directories as if the conversion has just completed.

        Ex.:
        ./test/work/test_delete
        ├── bbb.m4a
        ├── ccc.m4a
        └── out_conversion
            ├── bbb.flac
            └── ccc.flac
        """
        dir1 = self.ROOT_DIR
        dir1out = os.path.join(dir1, work_directory)
        os.mkdir(dir1out, mode=0o755)

        for name in ['bbb', 'ccc']:
            open(os.path.join(dir1, name + '.' + in_ext), 'w+').close()
            open(os.path.join(dir1out, name + '.' + out_ext), 'w+').close()

    def _validate_deletion(self, in_ext, out_ext, work_directory):
        """
        Validate:
            - Existence of files with output extension
            - Non-existence of files with input extension
            - Non-existence of work directory

        :param in_ext:
        :param out_ext:
        :param work_directory:
        """
        # Input files deleted, except if in_ext == out_ext (huge FLAC to FLAC 16-bit 44.1 kHz)
        if in_ext != out_ext:
            in_files = glob.glob(os.path.join(self.ROOT_DIR, '*.' + in_ext))
            self.assertEqual(len(in_files), 0, msg='Original input files must have been deleted')

        # Output files exist
        out_files = glob.glob(os.path.join(self.ROOT_DIR, '*.' + out_ext))
        self.assertEqual(len(out_files), 2, msg='Files with output extension must exist')

        # Work dir deleted
        self.assertFalse(os.path.exists(os.path.join(self.ROOT_DIR, work_directory)),
                         msg='Work directory must have been deleted')

    def test_delete_original_files(self):
        """
        For each support conversions:
            GIVEN a directory containing realistic files/directories at the end of a conversion
            WHEN the call to the function `delete_original_files` completes successfully
            THEN only files with the output extension exist (and no work directory)
        """
        for scenario in get_conversions():
            with self.subTest(msg=str(scenario)):
                self.setUp()

                in_ext = scenario[0]
                out_ext = scenario[1]

                self._create_files(in_ext, out_ext, WORK_DIRECTORY)

                # Call the function to test
                delete_original_files(self.ROOT_DIR, in_ext, out_ext, WORK_DIRECTORY)

                # Validate
                self._validate_deletion(in_ext, out_ext, WORK_DIRECTORY)

                # Delete files
                self.tearDown()


class TestCheckFFMPEGAvailable(unittest.TestCase):
    """
    Test `check_ffmpeg_available`.

    To detect if `ffmpeg` is available, `check_ffmpeg_available` calls
    `shutil.which('ffmpeg')`. This returns the path to `ffmpeg` if it is available,
    or None otherwise.

    We will use `unittest.mock.patch()` to simulate the 2 possible situations.
    """
    def test_ffmpeg_available(self):
        """
        Simulate that `ffmpeg` is available, using `unittest.mock.patch()`.
        """
        with patch.object(shutil, 'which', return_value='/path/to/ffmpeg') as mock_method:
            # nothing should happen here
            check_ffmpeg_available()

        mock_method.assert_called()

    def test_ffmpeg_not_available(self):
        """
        Simulate that `ffmpeg` is NOT available, using `unittest.mock.patch()`.
        """
        with patch.object(shutil, 'which', return_value=None) as mock_method:
            # not available, must raise an Error
            with self.assertRaises(OSError) as err:
                check_ffmpeg_available()

            the_exception = err.exception
            self.assertTrue('ffmpeg' in str(the_exception))

        mock_method.assert_called()


class TestCommandLineParsing(unittest.TestCase):
    """
    Test `parse_command_line`.
    """

    def _my_parse_cl_with_defaults(self, args):
        """
        :param args: command line arguments, except, input, output and bit rate
        :return: the object created by the command line parser (argparse.ArgumentParser)
        """
        return parse_command_line(args, DEFAULT_INPUT, DEFAULT_OUTPUT, DEFAULT_BIT_RATE)

    def test_no_arg_cause_system_exit(self):
        """
        GIVEN that not a single argument is passed in the command line
        WHEN  parsing the command line
        THEN  a SystemExit is raised because at least one option is mandatory
        AND   exit code is not zero
        """
        with self.assertRaises(SystemExit) as see:
            self._my_parse_cl_with_defaults([])

        exit_code = int(str(see.exception))
        self.assertEqual(1, exit_code)

    def test_delete_original_specified(self):
        """
        GIVEN `--delete_original` or `-d` passed in the command line
        WHEN  parsing the command line
        THEN  delete_original is True
        """
        for option in ['--delete_original', '-d']:
            with self.subTest(msg=option):
                args = self._my_parse_cl_with_defaults(['.', option])
                self.assertTrue(args.delete_original)

    def test_delete_original_default(self):
        """
        GIVEN `--delete_original` or `-d` are NOT passed in the command line
        WHEN  parsing the command line
        THEN  delete_original is False, its default value
        """
        args = self._my_parse_cl_with_defaults(['.'])
        self.assertFalse(args.delete_original)

    def test_default_extensions(self):
        """
        Test default values for input and output extensions.
        """
        args = self._my_parse_cl_with_defaults(['.'])
        self.assertEqual(args.input_extension, DEFAULT_INPUT)
        self.assertEqual(args.output_extension, DEFAULT_OUTPUT)

        for opt in ['--input_extension', '-i']:
            args = self._my_parse_cl_with_defaults(['.', opt, 'myInEx'])
            self.assertEqual(args.input_extension, 'myInEx')
            self.assertEqual(args.output_extension, DEFAULT_OUTPUT)

        for opt in ['--output_extension', '-o']:
            args = self._my_parse_cl_with_defaults(['.', opt, 'anOutEx'])
            self.assertEqual(args.input_extension, DEFAULT_INPUT)
            self.assertEqual(args.output_extension, 'anOutEx')

        args = self._my_parse_cl_with_defaults(['.', '-i', 'myInEx', '-o', 'anOutEx'])
        self.assertEqual(args.input_extension, 'myInEx')
        self.assertEqual(args.output_extension, 'anOutEx')

    def test_bit_rate_specified(self):
        """
        GIVEN `--bit_rate` or `-b` passed in the command line
        WHEN  parsing the command line
        THEN  the specified bit rate is considered
        """
        my_br = '222'
        for opt in ['--bit_rate', '-b']:
            with self.subTest(msg=opt):
                args = parse_command_line(['.', opt, my_br],
                                          DEFAULT_INPUT, DEFAULT_OUTPUT, DEFAULT_BIT_RATE)
                self.assertEqual(str(args.bit_rate), my_br)

    def test_bit_rate_not_specified(self):
        """
        GIVEN `--bit_rate` or `-b` are NOT passed in the command line
        WHEN  parsing the command line
        THEN  the default bit rate is taken
        """
        args = self._my_parse_cl_with_defaults(['.'])
        self.assertEqual(args.bit_rate, DEFAULT_BIT_RATE)

    def test_bit_rate_invalid_value(self):
        """
        GIVEN `--bit_rate` or `-b` passed in the command line, but NOT an integer
        WHEN  parsing the command line
        THEN  the specified bit rate is considered
        """
        my_br = '222kbps'
        for opt in ['--bit_rate', '-b']:
            with self.subTest(msg=opt):
                with self.assertRaises(SystemExit):
                    parse_command_line(['.', opt, my_br],
                                       DEFAULT_INPUT, DEFAULT_OUTPUT, DEFAULT_BIT_RATE)

    def test_show_command_specified(self):
        """
        GIVEN `--show_command` or `-sc` passed in the command line
        WHEN  parsing the command line
        THEN  show_command is True
        """
        for option in ['--show_command', '-sc']:
            with self.subTest(msg=option):
                args = self._my_parse_cl_with_defaults(['.', option])
                self.assertTrue(args.show_command)

    def test_show_command_not_specified(self):
        """
        GIVEN `--show_command` or `-sc` are NOT passed in the command line
        WHEN  parsing the command line
        THEN  show_command is False
        """
        args = self._my_parse_cl_with_defaults(['.'])
        self.assertFalse(args.show_command)

    def test_dry_run_specified(self):
        """
        GIVEN `--dry_run` or `-dr` passed in the command line
        WHEN  parsing the command line
        THEN  dry_ry is True
        """
        for option in ['--dry_run', '-dr']:
            with self.subTest(msg=option):
                args = self._my_parse_cl_with_defaults(['.', option])
                self.assertTrue(args.dry_run)

    def test_dry_not_specified(self):
        """
        GIVEN `--dry_run` or `-dr` are NOT passed in the command line
        WHEN  parsing the command line
        THEN  dry_ry is False
        """
        args = self._my_parse_cl_with_defaults(['.'])
        self.assertFalse(args.dry_run)


class TestGetAllDirectories(unittest.TestCase):

    """
    Test `get_all_directories` that must return all sub directories.

    test/test_get_all_directories/
    ├── foo
    ├── sub1
    │   ├── sub1a
    │   │   └── sub1a1
    │   │       ├── sub1a1a
    │   │       └── sub1a1b
    │   └── sub1b
    │       └── foo
    └── sub2

    """
    ROOT_DIR = os.path.join('.', 'test/test_get_all_directories')

    def test_get_all_directories(self):
        """
        Test `get_all_directories` on a directory containing many sub directories.
        """
        observed = get_all_directories(self.ROOT_DIR)

        observed = [str_a.replace(os.path.abspath('.'), '') for str_a in observed]

        expected = ['test/test_get_all_directories',
                    'test/test_get_all_directories/sub2',
                    'test/test_get_all_directories/sub1',
                    'test/test_get_all_directories/sub1/sub1a',
                    'test/test_get_all_directories/sub1/sub1b',
                    'test/test_get_all_directories/sub1/sub1a/sub1a1',
                    'test/test_get_all_directories/sub1/sub1a/sub1a1/sub1a1b',
                    'test/test_get_all_directories/sub1/sub1a/sub1a1/sub1a1a']

        self.assertEqual(len(expected), len(observed))
        self.assertEqual(expected.sort(), observed.sort())


class TestProcessDirectory(unittest.TestCase):
    """
    Test `process_directory`.
    """
    def test_with_audio_file_dry_run(self):
        """
        GIVEN a directory containing at least one audio file
        WHEN  calling `process_directory` on it
        THEN  it returns 1 because want to count directories containing audio files
        """
        exit_code = process_directory('test/mini_collection', 'flac', 'mp3',
                                      work_directory='my_work_dir',
                                      bit_rate=256,
                                      dry_run=True,
                                      delete_original=False)
        self.assertEqual(1, exit_code, msg='1 because an audio file exists there.')

    def test_without_audio_file_dry_run(self):
        """
        GIVEN a directory NOT containing any audio file
        WHEN  calling `process_directory` on it
        THEN  it returns 0 because want to count directories containing audio files
        """
        exit_code = process_directory(DIR_NO_AUDIO_FILE,
                                      'flac', 'mp3',
                                      work_directory='my_work_dir',
                                      bit_rate=256,
                                      dry_run=True,
                                      delete_original=False)
        self.assertEqual(0, exit_code, msg='0 because no audio file exists there.')

    def _sub_with_audio_file_delete(self, tmp_dir_name):
        """
        Create work directory and output file because the conversion will be mocked.
        :param tmp_dir_name: directory containing audio files
        :return: directory, audio_file, text_file, wrk_dir_full, my_work_dir, out_file
        """
        directory = os.path.abspath(tmp_dir_name)
        audio_file = os.path.join(directory, 'aaa.flac')
        os.mknod(audio_file)
        self.assertTrue(os.path.exists(audio_file))
        text_file = os.path.join(directory, 'aaa.txt')
        os.mknod(text_file)
        self.assertTrue(os.path.exists(text_file))

        # create artificial output file so the `delete_original` will be applied
        my_work_dir = 'my_work_dir'
        wrk_dir_full = os.path.join(directory, my_work_dir)
        os.makedirs(wrk_dir_full)

        out_file = 'aaa.mp3'
        out_file_full = os.path.join(directory, my_work_dir, out_file)
        os.mknod(out_file_full)
        self.assertTrue(os.path.exists(out_file_full))

        return directory, audio_file, text_file, wrk_dir_full, my_work_dir, out_file

    def test_audio_file_delete_mock(self):
        """
        GIVEN a directory containing at least one audio file
        AND   mocking the `ffmpeg` call
        WHEN  calling `process_directory` on it with `delete_original` to True
        THEN  it returns 1 because want to count directories containing audio files
        AND   the original audio file has been deleted
        AND   the output audio file has been moved from work directory into its parent directory
        AND   the work_directory has been deleted
        """
        with tempfile.TemporaryDirectory() as tmp_dir_name:
            directory, audio_file, text_file, wrk_dir_full, my_work_dir, \
                out_file = self._sub_with_audio_file_delete(tmp_dir_name)

            # mock `create_work_directory` so it does NOT erase the mp3 file
            with patch.object(convert_music, 'create_work_directory', return_value=wrk_dir_full) \
                    as mock_create_wd:
                # mock the OS call to `ffmpeg` for the conversion
                with patch.object(os, 'system', return_value=0) as mock_method:
                    exit_code = process_directory(directory, 'flac', 'mp3',
                                                  work_directory=my_work_dir,
                                                  bit_rate=256,
                                                  dry_run=False,
                                                  delete_original=True)
                    self.assertEqual(1, exit_code, msg='1 because an audio file exists there.')

                mock_method.assert_called()
                self.assertTrue(os.path.exists(text_file))
                self.assertFalse(os.path.exists(audio_file))
                self.assertTrue(os.path.exists(os.path.join(directory, out_file)))
                self.assertFalse(os.path.exists(wrk_dir_full))

            mock_create_wd.assert_called()

    def test_audio_file_no_delete_mock(self):
        """
        GIVEN a directory containing at least one audio file
        AND   mocking the `ffmpeg` call
        WHEN  calling `process_directory` on it with `delete_original` to False
        THEN  it returns 1 because want to count directories containing audio files
        AND   the original audio file still exists
        AND   the output audio file exists in the work directory
        """
        with tempfile.TemporaryDirectory() as tmp_dir_name:
            directory, audio_file, text_file, wrk_dir_full, my_work_dir, \
                out_file = self._sub_with_audio_file_delete(tmp_dir_name)

            # mock `create_work_directory` so it does NOT erase the mp3 file
            with patch.object(convert_music, 'create_work_directory', return_value=wrk_dir_full)\
                    as mock_create_wd:
                with patch.object(os, 'system', return_value=0) as mock_method:
                    exit_code = process_directory(directory, 'flac', 'mp3',
                                                  work_directory=my_work_dir,
                                                  bit_rate=256,
                                                  dry_run=False,
                                                  delete_original=False)
                    self.assertEqual(1, exit_code, msg='1 because an audio file exists there.')

                mock_method.assert_called()
                self.assertTrue(os.path.exists(text_file))
                self.assertTrue(os.path.exists(audio_file))
                self.assertTrue(os.path.exists(wrk_dir_full))
                self.assertTrue(os.path.exists(os.path.join(wrk_dir_full, out_file)))

            mock_create_wd.assert_called()


class TestCreateWorkDirectory(unittest.TestCase):
    """
    Test `create_work_directory`
    """
    def _sub_test_create_work_directory(self, tmp_full_name, wrk_name):
        """
        Call `create_work_directory(tmp_full_name, wrk_name)` and validate that the directory
        has been created.
        :param tmp_full_name: full name of the temp directory
        :param wrk_name: name of the work directory to create
        """
        full_wrk_name = os.path.join(tmp_full_name, wrk_name)

        create_work_directory(tmp_full_name, wrk_name)
        self.assertTrue(os.path.exists(full_wrk_name))

    def test_create_work_directory(self):
        """
        Call `create_work_directory` twice and validate all the expected states.
        """
        with tempfile.TemporaryDirectory() as tmp_dir_name:
            tmp_full_name = os.path.abspath(tmp_dir_name)
            wrk_name = 'myWorkDirName'
            full_wrk_name = os.path.join(tmp_full_name, wrk_name)
            self.assertFalse(os.path.exists(full_wrk_name))

            self._sub_test_create_work_directory(tmp_full_name, wrk_name)

            # create a file that should be deleted after
            my_file = os.path.join(full_wrk_name, 'my_file.txt')
            os.mknod(my_file)
            self.assertTrue(os.path.exists(my_file))

            self._sub_test_create_work_directory(tmp_full_name, wrk_name)
            # my_file must have been deleted
            self.assertFalse(os.path.exists(my_file))


class TestRunOsCommand(unittest.TestCase):
    """
    Test `run_os_command`.
    """
    def test_run_conversion(self):
        """
        Call with a simple and harmless command.
        """
        run_os_command('cd .')

    def test_os_call(self):
        """
        Validate that `os.system` is called with the given argument.
        """
        cmd = 'cd .'
        with patch.object(os, 'system', return_value=0) as mock_method:
            run_os_command(cmd)

        mock_method.assert_called_once_with(cmd)


class TestCheckConversionAvailable(unittest.TestCase):
    """
    Test `check_conversion_available`.
    """
    def test_available(self):
        """
        GIVEN an available conversion passed
        WHEN  calling `check_conversion_available`
        THEN  the call ends normally
        """
        check_conversion_available('m4a', 'flac')

    def test_unavailable(self):
        """
        GIVEN an unavailable conversion passed
        WHEN calling `check_conversion_available`
        THEN a SystemExit is raised
        """
        with self.assertRaises(SystemExit):
            check_conversion_available('foo', 'bar')


class TestMain(unittest.TestCase):
    """
    Test `main`.
    """
    def test_main_dry_run(self):
        """
        Call `main` with --dry_run.
        """
        exit_code = main(['.', '--dry_run'])
        self.assertEqual(0, exit_code)

    def test_main_show_command(self):
        """
        Call `main` with --show_command.
        """
        exit_code = main(['.', '--show_command', '--bit_rate', '32'])
        self.assertEqual(0, exit_code)

    def test_main_show_license(self):
        """
        Call `main` with --license.
        """
        with self.assertRaises(SystemExit) as see:
            main(['.', '--license', '--bit_rate', '32'])

        exit_code = int(str(see.exception))
        self.assertEqual(0, exit_code)

    def test_main_no_audio_file(self):
        """
        Call `main` on a folder without audio files.
        We use mock to simulate that `ffmpeg` is available to cover the call to
        `check_conversion_available`.
        """
        # Simulate that `ffmpeg` is available, using `unittest.mock.patch()`
        with patch.object(shutil, 'which', return_value='/path/to/ffmpeg') as mock_method:
            exit_code = main([DIR_NO_AUDIO_FILE, '--input_extension', 'flac',
                              '--output_extension', 'mp3'])
            self.assertEqual(0, exit_code)

        mock_method.assert_called()


class TestInit(unittest.TestCase):
    """
    Test `init`.
    """
    def test_init_not_calling_main(self):
        """
        Test that `init` does not call `main`, when we "directly" call `init`, and so the
        return code must be 0.
        """
        exit_code = convert_music.init()
        self.assertEqual(0, exit_code)

    def test_init_calling_main(self):
        """
        Test that `init` calls `main`, via a mock simulating a command line call like:
        `python convert_music.py`.
        """
        with patch.object(convert_music, 'main', return_value=42):
            # make sure a 'main' is called
            with patch.object(convert_music, '__name__', '__main__'):
                exit_code = convert_music.init()
                self.assertEqual(42, exit_code)

    def test_missing_root_dir_ok(self):
        """
        GIVEN that we use a mock to simulate a call without any argument
        WHEN  calling `init`
        THEN  the parsing fails because the mandatory argument(s) are not there
        AND   nothing special happens
        """
        # Use mock to set sys.argv to a single element, which corresponds to calling a script
        # without any argument
        script_name_only = ['the_script_name']
        with patch.object(sys, 'argv', script_name_only):
            # mock call to main
            with patch.object(convert_music, 'main', return_value=42):
                # make sure that 'main' is called
                with patch.object(convert_music, '__name__', '__main__'):
                    exit_code = convert_music.init()
                    self.assertEqual(42, exit_code)

    def test_invalid_argument_value(self):
        """
        GIVEN that we use a mock to simulate a call with an invalid value for an option
        WHEN  calling `init`
        THEN  the parsing fails because of the invalid value
        AND   the process ends with a SystemExit
        """
        # Use mock to set sys.argv to a single element, which corresponds to calling a script
        # without any argument
        script_name_only = ['the_script_name', '--bit_rate', 'should_be_integer']
        with patch.object(sys, 'argv', script_name_only):
            # make sure that 'main' is called
            with patch.object(convert_music, '__name__', '__main__'):
                # the process ends with a SystemExit
                with self.assertRaises(SystemExit):
                    convert_music.init()
