"""
Test many conversions on a mini collection.

Requirements:
 - ffmpeg
 - ffprobe
 - bash

The mini collection:
test/mini_collection/
├── aaa.flac
├── album1
│   ├── aaa.flac
│   └── bbb.flac
├── classical
│   ├── album2
│   │   ├── aaa.flac
│   │   └── bbb.flac
│   ├── album4
│   │   └── ccc.flac
│   ├── baroque
│   │   ├── album3
│   │   │   ├── aaa.flac
│   │   │   └── ddd.flac
│   │   └── no-audio-folder
│   └── no-audio-folder
├── credits-audio-file.txt
└── no-audio-folder
"""

import unittest
import os
import shutil
import tempfile
from convert_music import main, check_ffmpeg_available, WORK_DIRECTORY


NB_AUDIO_FILES = 8
SAMPLE_RATE = 44100
BITS_PER_RAW_SAMPLE = 16
BIT_RATE_MP3_DEFAULT = 320000
EXPECTED_PROP_FILE = 'expected_ffprobe.txt'


check_ffmpeg_available()

if shutil.which('ffprobe') is None:
    raise OSError('Command ffprobe is not available.')


def get_files_under(root_directory, file_extension):
    """
    Scan `root_directory`.
    :param root_directory: root directory to search
    :param file_extension: file extension of files to return
    :return: all files with a given extension, in any sub directory of `root_directory`
    """
    files = []
    for root, _, file_names in os.walk(root_directory):
        for filename in file_names:
            if filename.endswith(file_extension):
                files.append(os.path.join(root, filename))

    return files


def compare_output_properties_via_os(file, tmp_dir_name, expected_result_file):
    """
    :return: 0 only if the sample rate is the expected one.
    """
    # a destination file
    tmp_file = os.path.join(tmp_dir_name, 'observed_ffprobe.txt')

    return os.system('ffprobe {0} -show_streams -v quiet | '
                     'grep -E "^sample_rate=|^bits_per_raw_sample=|^codec_name=|^bit_rate=" | '
                     'sort > {1};'
                     'diff {1} {2}'.
                     format(file, tmp_file, expected_result_file))


def build_ffprobe_expected_output(sample_rate, bits_per_raw, codec_name, bit_rate):
    """
    Build text that will look like (in alphabetical order):
        bits_per_raw_sample=16
        codec_name=alac
        sample_rate=44100

    This will be used to validate the output files later.
    :return: the multi-line string
    """
    return 'bit_rate={}\nbits_per_raw_sample={}\ncodec_name={}\nsample_rate={}\n'.format(
        bit_rate, bits_per_raw, codec_name, sample_rate)


def remove_work_directories(root_directory, work_directory):
    """
    Remove all work directories found under `root_directory`.
    :param root_directory: root directory of the collection to clean up
    :param work_directory: work directory name
    """
    for root, _, _ in os.walk(os.path.abspath(root_directory)):
        if os.path.basename(root) == work_directory:
            shutil.rmtree(root)


class TestManyConversions(unittest.TestCase):
    """
    Test many conversions for real, if `ffmpeg` is available, on a mini collection.
    """

    ROOT = 'test/mini_collection/'

    def _assert_nb_audio_files(self, root_directory, file_extension, nb_files):
        """
        Assert that the number of files with `file_extension` anywhere under `root_directory`
        is equal to `nb_files`.
        :param root_directory: root directory of the tree to explore
        :param file_extension: extension of files to count
        :param nb_files: expected number of files
        """
        files = get_files_under(root_directory, file_extension)
        self.assertEqual(nb_files, len(files))

    def _validate_audio_properties(self, out_files, audio_properties):
        """
        Validate thoroughly the output files' audio properties
        :param out_files:
        """
        with tempfile.TemporaryDirectory() as tmp_dir_name:
            # build expected audio properties using ffprobe
            expected_output = os.path.join(tmp_dir_name, EXPECTED_PROP_FILE)
            ex_fd = open(expected_output, 'w')
            ex_fd.write(audio_properties)
            ex_fd.close()

            # Validate audio properties
            for out_file in out_files:
                with self.subTest(msg=out_file):
                    self.assertEqual(0, compare_output_properties_via_os(out_file,
                                                                         tmp_dir_name,
                                                                         expected_output))

    def _convert_and_validate(self,
                              in_ext,
                              out_ext,
                              audio_properties,
                              delete_original=False):
        """
        Call conversion command and validate the number of files, and the audio properties
        of the generated files.
        :param in_ext:
        :param out_ext:
        """
        # Make sure input files exist
        self._assert_nb_audio_files(self.ROOT, in_ext, NB_AUDIO_FILES)

        cmd = '{} -i {} -o {}'.format(self.ROOT, in_ext, out_ext)

        if delete_original:
            cmd += ' --delete_original'

        # Convert the mini collection
        main(cmd.split(' '))

        if delete_original is False:
            self._assert_nb_audio_files(self.ROOT, in_ext, NB_AUDIO_FILES)

        out_files = get_files_under(self.ROOT, out_ext)
        self.assertEqual(NB_AUDIO_FILES, len(out_files))

        self._validate_audio_properties(out_files, audio_properties)

    def test_01_flac_to_m4a(self):
        """
        Test flac to m4a.
        """
        in_ext = 'flac'
        out_ext = 'm4a'
        codec = 'alac'
        bits_per_raw_sample = BITS_PER_RAW_SAMPLE
        bit_rate = 455405

        audio_properties = build_ffprobe_expected_output(SAMPLE_RATE, bits_per_raw_sample, codec,
                                                         bit_rate)

        self._convert_and_validate(in_ext, out_ext, audio_properties, delete_original=True)

    def test_02_m4a_to_mp3(self):
        """
        Test m4a to mp3.
        """
        in_ext = 'm4a'
        out_ext = 'mp3'
        codec = 'mp3'
        bits_per_raw_sample = 'N/A'
        bit_rate = BIT_RATE_MP3_DEFAULT

        audio_properties = build_ffprobe_expected_output(SAMPLE_RATE, bits_per_raw_sample, codec,
                                                         bit_rate)

        self._convert_and_validate(in_ext, out_ext, audio_properties)

        remove_work_directories(self.ROOT, WORK_DIRECTORY)

    def test_03_m4a_to_flac(self):
        """
        Test m4a to flac.
        """
        in_ext = 'm4a'
        out_ext = 'flac'
        codec = 'flac'
        bits_per_raw_sample = BITS_PER_RAW_SAMPLE
        bit_rate = 'N/A'

        audio_properties = build_ffprobe_expected_output(SAMPLE_RATE, bits_per_raw_sample, codec,
                                                         bit_rate)

        self._convert_and_validate(in_ext, out_ext, audio_properties, delete_original=True)

    def test_04_flac_to_mp3(self):
        """
        Test flac to mp3.
        """
        in_ext = 'flac'
        out_ext = 'mp3'
        codec = 'mp3'
        bits_per_raw_sample = 'N/A'
        bit_rate = BIT_RATE_MP3_DEFAULT

        audio_properties = build_ffprobe_expected_output(SAMPLE_RATE, bits_per_raw_sample, codec,
                                                         bit_rate)

        self._convert_and_validate(in_ext, out_ext, audio_properties)

        remove_work_directories(self.ROOT, WORK_DIRECTORY)


class TestVariousFormatsSingleFile(unittest.TestCase):
    """
    Test conversions for real, if `ffmpeg` is available, on a single file.
    """

    ROOT = 'test/various_audio_formats'

    @classmethod
    def setUpClass(cls):
        remove_work_directories(cls.ROOT, WORK_DIRECTORY)

    @classmethod
    def tearDownClass(cls):
        cls.setUpClass()

    def __validate_audio_properties(self, out_file, audio_properties):
        """
        Validate thoroughly the output files' audio properties
        :param out_file: file to validate
        :param audio_properties: expected audio properties
        """
        with tempfile.TemporaryDirectory() as tmp_dir_name:
            # build expected audio properties using ffprobe
            expected_output = os.path.join(tmp_dir_name, EXPECTED_PROP_FILE)
            ex_fd = open(expected_output, 'w')
            ex_fd.write(audio_properties)
            ex_fd.close()

            # Validate audio properties
            self.assertEqual(0, compare_output_properties_via_os(out_file,
                                                                 tmp_dir_name,
                                                                 expected_output))

    def __convert_and_validate(self, in_ext, out_ext, root_local, in_bit_rate,
                               in_bits_per_raw_sample, in_codec, in_sample_rate, out_bit_rate,
                               out_bits_per_raw_sample, out_codec, out_sample_rate):
        # Check input file
        audio_properties = build_ffprobe_expected_output(in_sample_rate,
                                                         in_bits_per_raw_sample,
                                                         in_codec,
                                                         in_bit_rate)

        in_file = os.path.join(root_local, 'aaa.' + in_ext)
        self.__validate_audio_properties(in_file, audio_properties)

        audio_properties = build_ffprobe_expected_output(out_sample_rate,
                                                         out_bits_per_raw_sample,
                                                         out_codec,
                                                         out_bit_rate)

        main('{} -i {} -o {}'.format(root_local, in_ext, out_ext).split(' '))

        out_file = os.path.join(root_local, WORK_DIRECTORY, 'aaa.' + out_ext)
        self.__validate_audio_properties(out_file, audio_properties)

    def __get_standard_flac_properties(self):

        out_bit_rate = 'N/A'
        out_bits_per_raw_sample = BITS_PER_RAW_SAMPLE
        out_codec = 'flac'
        out_sample_rate = SAMPLE_RATE

        return out_bit_rate, out_bits_per_raw_sample, out_codec, out_sample_rate

    def test_flac_to_flac_reduce_size(self):
        """
        Test conversion of a huge FLAC file to a normal FLAC (Audio CD).
        """
        root_local = os.path.join(self.ROOT, 'flac-24-bit-192kHz')

        in_ext = 'flac'
        out_ext = 'flac'

        in_bit_rate = 'N/A'
        in_bits_per_raw_sample = 24
        in_codec = 'flac'
        in_sample_rate = 192000

        out_bit_rate, out_bits_per_raw_sample, out_codec, out_sample_rate = \
            self.__get_standard_flac_properties()

        self.__convert_and_validate(in_ext, out_ext, root_local, in_bit_rate,
                                    in_bits_per_raw_sample, in_codec, in_sample_rate, out_bit_rate,
                                    out_bits_per_raw_sample, out_codec, out_sample_rate)

        # check file size reduction
        in_size = os.stat(os.path.join(root_local, 'aaa.' + in_ext)).st_size
        out_size = os.stat(os.path.join(root_local, WORK_DIRECTORY, 'aaa.' + out_ext)).st_size
        self.assertTrue(out_size < in_size, msg='Output size must be smaller than input size')

    def __get_wav_16_48_properties(self):
        in_bit_rate = 1536000
        in_bits_per_raw_sample = 'N/A'
        in_codec = 'pcm_s16le'
        in_sample_rate = 48000

        return in_bit_rate, in_bits_per_raw_sample, in_codec, in_sample_rate

    def test_wav_to_mp3(self):
        """
        Test wav to mp3.
        """
        root_local = os.path.join(self.ROOT, 'wav-16-bit-48kHz')

        in_ext = 'wav'
        out_ext = 'mp3'

        in_bit_rate, in_bits_per_raw_sample, in_codec, in_sample_rate = \
            self.__get_wav_16_48_properties()

        out_bit_rate = BIT_RATE_MP3_DEFAULT
        out_bits_per_raw_sample = 'N/A'
        out_codec = 'mp3'
        out_sample_rate = SAMPLE_RATE

        self.__convert_and_validate(in_ext, out_ext, root_local, in_bit_rate,
                                    in_bits_per_raw_sample, in_codec, in_sample_rate, out_bit_rate,
                                    out_bits_per_raw_sample, out_codec, out_sample_rate)

    def test_wav_to_flac(self):
        """
        Test wav to flac.
        """
        root_local = os.path.join(self.ROOT, 'wav-16-bit-48kHz')

        in_ext = 'wav'
        out_ext = 'flac'

        in_bit_rate, in_bits_per_raw_sample, in_codec, in_sample_rate = \
            self.__get_wav_16_48_properties()

        out_bit_rate, out_bits_per_raw_sample, out_codec, out_sample_rate = \
            self.__get_standard_flac_properties()

        self.__convert_and_validate(in_ext, out_ext, root_local, in_bit_rate,
                                    in_bits_per_raw_sample, in_codec, in_sample_rate, out_bit_rate,
                                    out_bits_per_raw_sample, out_codec, out_sample_rate)


if __name__ == '__main__':
    unittest.main()
