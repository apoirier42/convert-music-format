"""
--
    Convert Music Format

    Copyright (C) 2019  Alain Poirier (alain.abc@gmail.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

Goal: Convert a complete music collection to a new format, ex.: FLAC to MP3.
"""

import argparse
import multiprocessing
import os
import sys
import glob
import shutil
from datetime import datetime
from time import time


DEFAULT_INPUT = 'm4a'
DEFAULT_OUTPUT = 'flac'
DEFAULT_BIT_RATE = 320
WORK_DIRECTORY = 'out_conversion'


def get_conversions_and_options(bit_rate):
    """
    Return dict of `input` -> `output` -> ffmpeg options, for all available conversions.

    :param bit_rate: bit rate, ex.: 320
    :return: a dict, for example:
             {'flac': {'m4a': ' -acodec alac ', 'flac': ' -acodec flac -sample_fmt s16 -ar 44100 '}
    """
    to_flac = ' -acodec flac -sample_fmt s16 -ar 44100 '
    to_mp3 = ' -acodec libmp3lame -b:a {}k -ar 44100 '.format(bit_rate)

    return {'flac': {'mp3': to_mp3,
                     'flac': to_flac,
                     'm4a': ' -acodec alac '},
            'm4a': {'mp3': to_mp3,
                    'flac': to_flac},
            'wav': {'mp3': to_mp3,
                    'flac': to_flac}}


def get_conversions():
    """
    :return: all the possible conversions is a tuple of tuples, ex.:
              (('flac', 'mp3'), ('flac', 'm4a'))
    """
    # bit_rate has no impact here:
    conversions = get_conversions_and_options(bit_rate=42)

    mappings = []
    for key1 in conversions.keys():
        for key2 in conversions[key1].keys():
            mappings.append((key1, key2))

    return tuple(mappings)


def check_conversion_available(input_extension, output_extension):
    """
    If conversion from `input_extension` to `output_extension` is not available,
    EXIT with code 2.
    :param input_extension: input extension, ex.: m4a
    :param output_extension: output extension, ex.: flac
    """
    conversions = get_conversions()
    if (input_extension, output_extension) not in conversions:
        print('\nAvailable conversions: {}'.format(conversions))
        print('\nSTOP: Input "{}" to output "{}" is not available.'.
              format(input_extension, output_extension))
        sys.exit(2)


def check_ffmpeg_available():
    """
    If command `ffmpeg` is not available, an OSError is raised.
    """
    if shutil.which('ffmpeg') is None:
        raise OSError('Command ffmpeg is not available.')


def _sub_build_ffmpeg_command(output_folder,
                              directory,
                              ffmpeg_options,
                              input_extension,
                              output_extension):
    """
    Construct the ffmpeg command line.
    :param output_folder:    output folder as absolute path
    :param directory:        directory containing input audio files
    :param ffmpeg_options: options to add to the call to `ffmpeg` ex.: ' -acodec flac '
    :param input_extension: input extension, ex.: m4a
    :param output_extension: output extension, ex.: flac
    :return: a string containing all the OS commands to convert all audio files of a folder
    """
    cmd = 'cd "' + os.path.abspath(directory) + '"; ' + \
          'for f in *.' + input_extension + \
          '; do ffmpeg -i "$f" -loglevel quiet ' + ffmpeg_options + ' "' + \
          output_folder + '"/"${f%.' + input_extension + '}.' + \
          output_extension + '"; done'

    return cmd


def build_ffmpeg_command(output_folder, directory, input_extension, output_extension, bit_rate):
    """
    :param output_folder:    output folder as absolute path
    :param directory:        directory containing input audio files
    :param input_extension: input extension, ex.: m4a
    :param output_extension: output extension, ex.: flac
    :param bit_rate: bit rate (kbps), ex.: 320
    :return: a complete `ffmpeg` command to call at the OS level
    """
    check_conversion_available(input_extension, output_extension)

    conversions = get_conversions_and_options(bit_rate)

    options = conversions[input_extension][output_extension]

    cmd = _sub_build_ffmpeg_command(output_folder, directory, options,
                                    input_extension, output_extension)

    return cmd


def create_work_directory(directory, work_directory):
    """
    Create a work directory into `directory`

    :param directory: directory to convert
    :param work_directory: name of work directory to create, into `directory`
    :returns: the `work_directory` full path
    """
    directory = os.path.abspath(directory)

    # Work folder remove / make
    work_dir_local = os.path.join(directory, work_directory)
    if os.path.exists(work_dir_local):
        shutil.rmtree(work_dir_local)

    os.mkdir(work_dir_local, mode=0o755)

    return work_dir_local


def delete_original_files(directory, input_extension, output_extension, work_directory):
    """
    Delete original files of given `directory`, move new ones to `directory`, and
    delete work directory.

    :param directory: directory containing original files
    :param input_extension: input extension, ex.: m4a
    :param output_extension: output extension, ex.: flac
    :param work_directory: name of work directory to create, into `directory`
    """
    work_dir_local = os.path.join(directory, work_directory)

    # if new files in `work_dir_local`
    new_files = glob.glob(os.path.join(work_dir_local, '*.' + output_extension))
    if len(new_files) > 0:
        # Delete original files
        for file_name in os.listdir(directory):
            if file_name.endswith(input_extension):
                os.remove(os.path.join(directory, file_name))

        # Move new files to `directory`
        for file_name in os.listdir(work_dir_local):
            if file_name.endswith(output_extension):
                shutil.move(os.path.join(work_dir_local, file_name),
                            os.path.join(directory, file_name))

        os.rmdir(work_dir_local)


def get_all_directories(root_directory):
    """
    :param root_directory: root directory of the music collection.
    :return: all sub directories and `root_directory`
    """
    all_directories = [os.path.abspath(root_directory)]

    for root, directories, _ in os.walk(root_directory):
        [all_directories.append(os.path.abspath(os.path.join(root, directory)))
         for directory in directories]

    return all_directories


def run_os_command(os_commands):
    """
    Call the OS to run the necessary commands, including `ffmpeg`, to convert
    all the audio files of a directory.
    :param os_commands: OS commands to convert all audio files of a directory, in a single string.
    """
    os.system(os_commands)


def process_directory(directory,
                      input_extension,
                      output_extension,
                      work_directory,
                      bit_rate,
                      dry_run,
                      delete_original) -> int:
    """
    Convert audio files of this folder.

    :param directory: a directory potentially containing audio files to convert (full path)
    :param input_extension: input extension, ex.: m4a
    :param output_extension: output extension, ex.: flac
    :param work_directory: name of work directory where output files are created
    :param bit_rate: bit rate (kbps), ex.: 320
    :param dry_run: execute only a dry-run T/F
    :param delete_original: delete original files T/F
    :returns: 0 if no audio files were detected, 1 otherwise. This way we can count how many
             directories have been modified.
    """
    audio_files = glob.glob(os.path.join(directory, '*.' + input_extension))

    # If NO audio file, exit this function
    if len(audio_files) == 0:
        return 0

    print(os.path.abspath(directory))

    if dry_run:
        return 1

    work_dir_local = create_work_directory(directory, work_directory)

    cmd = build_ffmpeg_command(work_dir_local, directory,
                               input_extension, output_extension, bit_rate)

    run_os_command(cmd)

    if delete_original:
        delete_original_files(directory, input_extension, output_extension, work_directory)

    return 1


def get_license_text():
    return '''        Convert Music Format\n
        Copyright (C) 2019  Alain Poirier (alain.abc@gmail.com)

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <http://www.gnu.org/licenses/>.'''


def parse_command_line(command_line_arguments, default_input, default_output, default_bit_rate):
    """
    :param command_line_arguments: list of arguments passed via the command line
    :param default_input: default input extension, ex.: m4a
    :param default_output: default output extension, ex.: flac
    :param default_bit_rate: default bit rate (kbps), ex.: 320
    :return: object created by parser.parse_args(command_line_arguments)
    """
    avail_conversions = ''
    for el in sorted(get_conversions()):
        avail_conversions += str.format('\n %25s to %-5s' % el)

    parser = argparse.ArgumentParser(
        'Convert audio files to a new\n' +
        '       format (ex.: ALAC to FLAC), recursively, from a root directory.\n\n' +
        'Author: Alain Poirier (alain.abc@gmail.com), 2019.\n',
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog='Available conversions: {}'.format(avail_conversions))

    # mandatory arguments
    parser.add_argument('root_directory', nargs='?',
                        help='Root directory of the music collection')

    # optional arguments (having default values)
    parser.add_argument('-d', '--delete_original', action='store_true',
                        help='Delete original files. Default: False')

    parser.add_argument('-i', '--input_extension', default=default_input,
                        help='Extension of the files to convert. Default: {}'.format(default_input))

    parser.add_argument('-o', '--output_extension', default=default_output,
                        help='Extension of the files to create. Default: {}'.format(default_output))

    parser.add_argument('-b', '--bit_rate', default=default_bit_rate, type=int,
                        help='Bit rate (kbit/s), ex.: 128, 160, 192, 256, 320. Used when ' +
                        'converting to MP3 for example. Default: {}'.format(default_bit_rate))

    parser.add_argument('-dr', '--dry_run', action='store_true',
                        help='Print the directories that would be processes, and the conversion ' +
                        'command and stop.')

    parser.add_argument('-sc', '--show_command', action='store_true',
                        help='Print the conversion command and stop.')

    parser.add_argument('-li', '--license', action='store_true',
                        help='Print license info.')

    args = parser.parse_args(command_line_arguments)

    if args.license:
        print(get_license_text())
        exit(0)

    if args.root_directory is None:
        print('The root_directory argument is mandatory.\nUse -h to see help.')
        exit(1)

    return args


def main(args):
    """
    The main entry point.
    :param args: the command line arguments
    :return: 0 if everything went well
    """
    args = parse_command_line(args, DEFAULT_INPUT, DEFAULT_OUTPUT, DEFAULT_BIT_RATE)

    datetime_format = '%Y-%M-%d %H:%m:%S'

    start = time()

    root_directory = os.path.abspath(args.root_directory)
    delete_original = args.delete_original
    input_extension = args.input_extension
    output_extension = args.output_extension
    bit_rate = args.bit_rate
    dry_run = args.dry_run
    show_command = args.show_command

    print('\n{}\n'.format(datetime.now().strftime(datetime_format)))
    print('Root folder:     {}'.format(root_directory))
    print('Delete original: {}'.format(delete_original))
    print('Input format:    {}'.format(input_extension))
    print('Output format:   {}'.format(output_extension))

    check_conversion_available(input_extension, output_extension)

    if bit_rate != DEFAULT_BIT_RATE:
        print('Bit rate (kbps): {}'.format(bit_rate))

    print()

    if show_command or dry_run:
        print('Here is the complete conversion command corresponding to your options, ' +
              'for a single directory:\n')
        cmd = build_ffmpeg_command(WORK_DIRECTORY, root_directory,
                                   input_extension, output_extension, bit_rate)
        print('{}\n'.format(cmd))

        if show_command:
            print('Exit.')
            return 0
    else:
        check_ffmpeg_available()

    # Loop over all subdirectories
    directories = get_all_directories(root_directory)

    # number of cores to use in parallel
    nb_cpus = max(1, os.cpu_count() - 1)

    # Run in parallel on all directories
    with multiprocessing.Pool(processes=nb_cpus) as pool:
        results = [pool.apply_async(func=process_directory,
                                    args=(directory, input_extension, output_extension,
                                          WORK_DIRECTORY, bit_rate, dry_run, delete_original))
                   for directory in directories]
        join = [res.get(timeout=60 * 20) for res in results]

        print('\nDirectories processed: {}'.format(sum(join)))

    print('\n{}'.format(datetime.now().strftime(datetime_format)))
    print('\nTook {}'.format(datetime.utcfromtimestamp(time() - start).strftime('%Hh %Mm %Ss')))
    return 0


def init():
    """
    Call `main`.
    :return: `main`'s exit code if called, 0 otherwise.
    """
    if __name__ == "__main__":
        return main(sys.argv[1:])

    return 0


init()
