<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Convert Music Format](#convert-music-format)
	- [Features](#features)
		- [Convert audio format](#convert-audio-format)
		- [Parallel processing](#parallel-processing)
		- [Reduce audio file size](#reduce-audio-file-size)
			- [Lossless to lossy](#lossless-to-lossy)
			- [Lossless to lossless](#lossless-to-lossless)
		- [Available conversions](#available-conversions)
	- [Get started](#get-started)
	- [Technologies](#technologies)
	- [Help](#help)
	- [Credits](#credits)

<!-- /TOC -->

# Convert Music Format

Convert a complete music collection from a format to another one, ex. FLAC to MP3 160 kbps.

## Features

### Convert audio format

It is possible to create a copy of your lossless audio files into another format,
lossless or not.

### Parallel processing

The conversion of many albums (assuming one directory per album) is
launched in parallel. For an album, the process is serial.

### Reduce audio file size

To reduce audio file size is an objective, but in some cases, it can even
be done without any impact on the perceived audio quality, i.e., lossless
to lossless format). Let's start with the situation where we accept some
audio quality reduction.

#### Lossless to lossy

It is possible to convert lossless [FLAC](https://en.wikipedia.org/wiki/FLAC)
files to [MP3](https://en.wikipedia.org/wiki/MP3), so it uses less space
on a mobile device for example.

#### Lossless to lossless

Without any loss, it is possible to convert for example from **m4a** 16-bit 44.1 kHz
to **flac** 16-bit 44.1 kHz. The file size will be similar.

For audio files "beyond"
[Audio CD](https://en.wikipedia.org/wiki/Compact_Disc_Digital_Audio) specifications
(16-bit and [44.1 kHz](https://en.wikipedia.org/wiki/44,100_Hz)),
then it is most probably possible to create a smaller version of them, without any
[perceptible](https://en.wikipedia.org/wiki/44,100_Hz#Human_hearing_and_signal_processing) audio
quality loss.

Ex.:<br>
FLAC 24-bit 96000 Hz to <br>
FLAC 16-bit 44100 Hz

### Available conversions

All the available conversions are displayed via [help](#help).

At least those conversions are available:

                      flac to flac
                      flac to m4a
                      flac to mp3
                       m4a to flac
                       m4a to mp3
                       wav to flac
                       wav to mp3

## Get started

* Have all the following [technologies](#technologies) installed.
* Download [convert_music.py](convert_music.py).
* See [Help](#help).

## Technologies

* [Python 3](https://www.python.org/) &nbsp;&nbsp; <img src="icon-python-powered-w-140x56.png"
    height=35/>
* [FFmpeg](https://www.ffmpeg.org/)
* [Bash](https://en.wikipedia.org/wiki/Bash_(Unix_shell))

## Help

To see help, in the console, execute:

`python3 convert_music.py -h`

## Credits

Icon <img src="music.png" height=40/> made by
[Pixel Buddha](https://www.flaticon.com/authors/pixel-buddha) from
[Flaticon](https://www.flaticon.com/).
